/* Collect all .btn classes into a NodeList and comvert it into
|| an array
*/
// var btnGroup = Array.from(document.getElementsByClassName("btn"));

/* Collect all .img classes into a NodeList and convert it into
|| an array
*/
// var imgGroup = Array.from(document.getElementsByClassName('image'));

/* Iterate (loop) thru the imgGroup array with forEach() array
|| method.
*/// In each loop get a .btn from the btnGroup array's index
//// position that corresponds with the current index of the loop.
//// Register an onclick event handler that toggles the .off class
//// to a .img of the imgGroup array positioned at current loop
//// index.


// imgGroup.forEach(function(img, idx) {
//
//   btnGroup[idx].onclick = function() {
//
//     img.classList.toggle('off');
//   }
//
// });
'use strict';

/**
 * Makes an element draggable.
 *
 * @param {HTMLElement} element - The element.
 */
function draggable(element) {
  var isMouseDown = false;

  // initial mouse X and Y for `mousedown`
  var mouseX;
  var mouseY;

  // element X and Y before and after move
  var elementX = 0;
  var elementY = 0;

  // mouse button down over the element
  element.addEventListener('mousedown', onMouseDown);

  /**
   * Listens to `mousedown` event.
   *
   * @param {Object} event - The event.
   */
  function onMouseDown(event) {
    mouseX = event.clientX;
    mouseY = event.clientY;
    isMouseDown = true;
  }

  // mouse button released
  element.addEventListener('mouseup', onMouseUp);

  /**
   * Listens to `mouseup` event.
   *
   * @param {Object} event - The event.
   */
  function onMouseUp(event) {
    isMouseDown = false;
    elementX = parseInt(element.style.left) || 0;
    elementY = parseInt(element.style.top) || 0;
  }

  // need to attach to the entire document
  // in order to take full width and height
  // this ensures the element keeps up with the mouse
  document.addEventListener('mousemove', onMouseMove);

  /**
   * Listens to `mousemove` event.
   *
   * @param {Object} event - The event.
   */
  function onMouseMove(event) {
    if (!isMouseDown) return;
    var deltaX = event.clientX - mouseX;
    var deltaY = event.clientY - mouseY;
    element.style.left = elementX + deltaX + 'px';
    element.style.top = elementY + deltaY + 'px';
  }
}

let boxes = document.querySelectorAll(".box");

for(let i = 0; i < boxes.length; i++) {

	const box = boxes[ i ];
	draggable( box );

}
